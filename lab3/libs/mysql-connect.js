/**
 * Created by yevhenii on 11/2/16.
 */

var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'yevhenii',
    password: 'yevhenii',
    database: 'lab3'
});

connection.connect();

module.exports = connection;