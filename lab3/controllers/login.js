/**
 * Created by yevhenii on 11/2/16.
 */
var connection = require('./../libs/mysql-connect');

module.exports.loginGet = function(req, res){
    if(req.session.loggedin){
        return res.redirect('/');
    }
    return res.render('login');
};

module.exports.loginPost = function(req, res){
    var email = req.body.email;
    var password = req.body.pass;
    var query = 'SELECT * FROM user WHERE email=\'' + email + '\' AND password=\'' + password + '\';';

    connection.query(query, function(err, rows, fields){
        if(err){
            return res.render('error');
        }
        if(rows.length === 0){
            return res.render('login', {err: 'User with entered email and password does not exist!'});
        }
        req.session.loggedin = true;
        return res.redirect('/');
    });
};