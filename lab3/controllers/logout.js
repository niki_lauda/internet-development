/**
 * Created by yevhenii on 11/2/16.
 */

module.exports.logoutGet = function(req, res){
  req.session.destroy(function(err){
      if(err){
          console.error(err);
      }
      return res.redirect('/');
  })
};