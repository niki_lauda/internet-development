/**
 * Created by yevhenii on 11/2/16.
 */

var connection = require('./../libs/mysql-connect');

module.exports.patientsGet = function(req, res){
    var query = 'SELECT patient.pname, room.rnum, department.dtype,  doctor.docname FROM room INNER JOIN department' +
    ' ON room.depid = department.did INNER JOIN doctor ON doctor.depid = department.did INNER JOIN registry'+
    ' ON registry.roomid = room.rid INNER JOIN patient ON registry.patid = patient.pid;';

    connection.query(query, function(err, rows){
        if(err){
            return res.render('error', {'message': 'Database Error!'});
        }
        return res. render('patients', {'data': rows});
    });
};