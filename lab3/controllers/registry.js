/**
 * Created by yevhenii on 11/2/16.
 */

var connection = require('./../libs/mysql-connect');

module.exports.registryGet = function(req, res){
    var query = 'SELECT * FROM registry;';
    connection.query(query, function(err, rows){
        if(err){
            return res.render('error', {'message': 'Database Error!'});
        }
        return res.render('registry', {'data': rows});
    });
};

module.exports.registryPost = function(req, res){
    console.log(req.body.regid);
    var regid = req.body.regid;
    var patid = req.body.patid;
    var docid = req.body.docid;
    var roomid = req.body.roomid;
    var query = '';

    if(!regid && !patid && !docid && !roomid){
        return res.render('error', {'message': 'All fields are empty! Nothing to do!'});
    }

    if(!regid && patid && docid && roomid){
        query = 'INSERT INTO registry(patid, docid, roomid) VALUES('+
                '\'' + patid + '\', \'' + docid + '\',\'' + roomid + '\');';
    }
    else if(regid && !patid && !docid && !roomid){
        query = 'DELETE FROM registry WHERE regid=\'' + regid + '\';';
    }
    else if(regid && patid && docid && roomid){
        query = 'UPDATE registry SET patid=\'' + patid + '\', docid=\'' + docid + '\', roomid=\'' + roomid + '\' WHERE regid=\'' + regid + '\';';
    }

    connection.query(query, function(err, rows){
        if(err){
            return res.render('error', {'message': 'Database Error!'});
        }
        return res.redirect('/registry');
    });
};