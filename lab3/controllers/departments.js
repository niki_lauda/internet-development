/**
 * Created by yevhenii on 11/2/16.
 */

var connection = require('./../libs/mysql-connect');

module.exports.departmentsGet = function(req, res){
    var query = 'SELECT * FROM department;';
    connection.query(query, function(err, rows){
        if(err){
            return res.render('error', {'message': 'Database Error!'});
        }
        return res. render('departments', {'data': rows});
    });
};

module.exports.departmentsGetOne = function(req, res){
    var query  = 'SELECT room.rid, room.capacity, room.rnum FROM room INNER JOIN department' +
    ' ON room.depid=department.did WHERE room.depid=\'' + req.params.id +'\';';
    connection.query(query, function(err, rows){
        if(err){
            return res.render('error', {'message': 'Database Error!'});
        }
        return res. render('departmentSingle', {'depid': req.params.id, 'data': rows});
    });
};