/**
 * Created by yevhenii on 11/2/16.
 */

var connection = require('./../libs/mysql-connect');

module.exports.dutiesGet = function(req, res){
    var query = 'SELECT duty.day, doctor.docname, department.dtype FROM duty' +
    ' INNER JOIN doctor ON duty.docid = doctor.docid INNER JOIN department ON duty.depid = department.did;';
    console.log(query);
    connection.query(query, function(err, rows){
        if(err){
            return res.render('error', {'message': 'Database Error!'});
        }
        return res. render('duties', {'data': rows});
    });
};

module.exports.dutiesOnDay = function(req, res){
    var query = 'SELECT doctor.docname, department.dtype' +
    ' FROM duty INNER JOIN doctor ON duty.docid = doctor.docid INNER JOIN department' +
    ' ON duty.depid = department.did WHERE day=\'' + req.params.day + '\';';
    connection.query(query, function(err, rows){
        if(err){
            return res.render('error', {'message': 'Database Error!'});
        }
        return res. render('dutiesOnDay', {'day': req.params.day, 'data': rows});
    });
};