/**
 * Created by yevhenii on 11/2/16.
 */

var connection = require('./../libs/mysql-connect');

module.exports.registrationGet = function(req, res){
    if(req.session.loggedin){
        return res.redirect('/');
    }
    return res.render('registration');
};

module.exports.registrationPost = function(req, res){
    var data = req.body;
    var firstName = data.name1;
    var secName = data.name2;
    var faculty = data.facult;
    var email = data.email;
    var pass = data.pass;

    var queryInsert = 'INSERT INTO user(name, surname, faculty, email, password)' +
        ' VALUES(\'' + firstName + '\', \'' + secName + '\', \'' + faculty
        + '\', \'' + email + '\', \'' + pass + '\');';
    var queryCheckExists = 'SELECT * FROM user WHERE email=\'' + email + '\';';
    connection.query(queryCheckExists, function(err, rows){
        if(err){
            return res.render('registration', {err: 'Database Error!'});
        }
        if(rows.length !== 0){
            return res.render('registration', {err: 'User with entered email and password already exists!'});
        }


        connection.query(queryInsert, function(err, rows){
            if(err){
                return res.render('registration', {err: 'Database Error!'});
            }
            return res.render('home', {message: 'You are registered! Please log in to continue'});
        });

    });


};