/**
 * Created by yevhenii on 10/28/16.
 */
var express = require('express');
var handlebars = require('express-handlebars');
var app = express();
var bodyParser = require('body-parser');
var routes = require('./routes');
var connection = require('./libs/mysql-connect');
var session = require('express-session');

app.engine('handlebars', handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {}
}));

app.use(function(req, res, next){
    res.locals.loggedin = req.session.loggedin;
    next();
});

app.use(function(req, res, next){
    var allowedPathUnlogged = ['/login', '/loginPost', '/', '/registration', '/registrationPost'];
    if(!req.session.loggedin && allowedPathUnlogged.indexOf(req.url) < 0){
        req.url = '/';
    }
    next();

});

routes.registerRoutes(app);

process.on('SIGTERM', function(){
    console.log('MySQL connection closed!');
    connection.end();
});

app.listen(3000);
