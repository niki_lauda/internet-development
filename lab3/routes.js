/**
 * Created by yevhenii on 11/2/16.
 */

var loginRoute = require('./controllers/login');
var registrationRoute = require('./controllers/registration');
var logoutRoute = require('./controllers/logout');
var homeRoute = require('./controllers/home');
var departmentsRoute = require('./controllers/departments');
var patientsRoute = require('./controllers/patients');
var dutiesRoute = require('./controllers/duties');
var registryRoute = require('./controllers/registry');


module.exports.registerRoutes = function(app){
    app.get('/', homeRoute.homeGet);

    app.get('/login', loginRoute.loginGet);
    app.post('/loginPost', loginRoute.loginPost);

    app.get('/registration', registrationRoute.registrationGet);
    app.post('/registrationPost', registrationRoute.registrationPost);

    app.get('/logout', logoutRoute.logoutGet);

    app.get('/departments', departmentsRoute.departmentsGet);
    app.get('/departments/:id', departmentsRoute.departmentsGetOne);

    app.get('/patients', patientsRoute.patientsGet);

    app.get('/duties', dutiesRoute.dutiesGet);
    app.get('/duties/:day', dutiesRoute.dutiesOnDay);

    app.get('/registry', registryRoute.registryGet);
    app.post('/registryPost', registryRoute.registryPost);
};